FROM ubuntu:14.04

#Install nodejs
RUN apt-get update && apt-get install -y nodejs npm
RUN update-alternatives --install /usr/bin/node node /usr/bin/nodejs 10

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Install app dependencies
COPY package.json /usr/src/app/
RUN npm install

# Bundle app source
COPY . /usr/src/app

RUN ls -l

EXPOSE 3000

CMD [ "npm", "start" ]