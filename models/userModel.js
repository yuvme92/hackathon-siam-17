var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

mongoose.connect('mongodb://mongo:27017/hackathon');

var userSchema = new Schema({
	'name' : String
});

module.exports = mongoose.model('user', userSchema);
